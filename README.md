# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Modifications done by Elyse Smolkowski, esmolkow@uoregon.edu

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  


### Usage ###

* Clone the repo.

* Run `docker build -t uocis-brevets .` from the `brevets` directory.

* Next, run `docker run -d -p 5000:5000 uocis-brevets`.

* To run the provided test suite, enter the brevets directory and type 'nosetests'

## Interpretation of ACP

The rules are followed exactly as stateed on https://rusa.org/pages/acp-brevet-control-times-calculator and https://rusa.org/pages/rulesForRiders.

Specifically Article 9 which sets max times for given bravets is of interest and required modifications to the base code. It is assumed that all checkpoints are located within the same time zone.

Another detail that required additional attention was the timezone of the client vs the server. Time calculations are all entered in, and performed in UTC if that matters.